Connor Patrick
PTRCON001
March 2013
CSC3020H
XNA Basics Assignment

Description:
This game is a simple two-player clone of the game Battle City. 

 2-Players: 
	This game does support two players.
 
 XBox Support:
	This game does run on XBox 360
	
Collision System:
	The game makes use of a simple grid system to handle collisions between moving objects.
	Moving objects colliding with stationary objects is done by getting the map coordinates
	of the moving objects and checking the value of the terrain in the map array; this seemed
	the most efficient way. All sprites make use of bounding boxes for collision.
	
Basic AI:
	The AI in this game is incredibly basic. They shoot on a set timer and when they collide 
	with a wall they randomly choose a direction and try again. If they hit another enemy they 
	simply turn around. If they hit a player then they retreat; this acts as defensive behaviour.
	
Menu System:
	The game features a main menu at game initialization as well as an in-game menu, (which does
	pause the game). Ending the game from the in-game menu sends the players back to the main menu.
	Each menu has two options, the button with the red text and the two tanks on it is the one 
	that is currently selected. 
	
Winning/Losing conditions:
	The game is over if an enemy manages to shoot the players' base, (which is the bird emblem at
	the bottom). In this case the player's scores are compared to determine a winner. The game will
	also end if one player runs out of lives, in which case the other player wins. The objective is 
	to destroy all enemy tanks without dieing or letting them destroy the base.
	
Enjoyment:
	(I have made it fairly difficult as I prefer for games to be a challenge)
	To avoid unnecessary frustration players can move and shoot through each other. Players also
	can not destroy their own base. 
	
Extras:
	- Music for the menu
	- In-game music
	- Sound effects
	- Varying terrain (water, brick, stone, forest)
	- Various enemies (noraml, fast, strong)
	- Animated explosion
	- Decent visuals
	
Instructions:
	Main Menu:
		"Start Game" to start a new game
		"Exit Game" to exit program
		Player 1 has control of the menu. "A" with gamepad to select, "Enter" with keyboard
	In-game:
			-Gamepad:
				Left joystick or DPad to change direction of motion. Player moves by default.
				Hold left trigger to stop moving, release to continue.
				"A" fires a projectile. There is a reload delay.
				"Start" pauses the game
			-Keyboard:
				Player 1 uses "W", "A", "S", "D" to change direction, left "CTRL" to stop moving and "SPACE" to fire
				Player 2 uses "Up", "Left", "Right", "Down" tp change direction, right "CTRL" to stop moving and "ENTER" to fire
				"ESCAPE" pauses the game
			-Game rules:
				Players do not collide, nor do their projectiles hit each other
				Enemy bullets do not collide with other enemies or each other
				Water can be shot over but not driven through
				Brick can be destroyed
				Stone can not be destroyed
				Trees are above the battle field; they can be shot under and driven under
				There can only be 10 enemies on screen at any given time
								
	In-game Menu:
		"Resume Game" to continue playing.
		"End Game" to return to main menu.
	Game Over Screen:
		Press "ESCAPE" on keyboard to return to main menu
		Press "Start" on gamepad to return to main menu
				