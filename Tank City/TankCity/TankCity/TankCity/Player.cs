﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TankCity
{
    class Player : MovingObject
    {
        //Initial position (spawn position)
        public Vector2 InitPosition;
        //List of textures
        public List<Texture2D> textures = new List<Texture2D>();

        //Health of player
        public int Lives;

        //Player score
        public int Score;

        //Reload delay
        public int Reload = 40;

        public void Initialize(Vector2 position, int width, int height)
        {
            InitPosition = position;
            Position = InitPosition;
            Width = width;
            Height = height;
            Score = 0;
            Velocity = Vector2.Zero;
            Speed = 1;
            Direction = 1;
            Lives = 5;
            ObjectType = 1;
        }
    }
}
