﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace TankCity
{
    class Explosion
    {
        //sprite strip
        Texture2D spriteStrip;

        //time since last frame
        int elapsedTime;

        //time between frames
        int frameTime;

        //number of frames
        int frames;

        //index of current frame
        int currentFrame;

        //part of strip to display
        Rectangle sourceRect = new Rectangle();

        //destination rectangle
        Rectangle destinationRect = new Rectangle();

        //Dimensions of frames
        public int width;
        public int height;

        //position of explosion
        public Vector2 Position;

        //state of animation
        public bool finished;

        //Initializing the explosion object
        public void Initialize(Texture2D texture, Vector2 position, int fWidth, int fHeight, int fCount, int fTime)
        {
            spriteStrip = texture;
            Position = position;
            width = fWidth;
            height = fHeight;
            frames = fCount;
            frameTime = fTime;
            elapsedTime = 0;
            currentFrame = 0;
            finished = false;
        }
        
        //Updating the explosion object
        public void Update(GameTime gameTime)
        {
            elapsedTime += (int)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedTime > frameTime)
            {
                currentFrame++;
                if (currentFrame == frames)
                    finished = true;
                elapsedTime = 0;
            }

            sourceRect = new Rectangle(currentFrame * width, 0, width, height);
            destinationRect = new Rectangle((int)Position.X, (int)Position.Y, 32, 32);
        }

        //Drawing the explosion
        public void Draw(SpriteBatch spriteBatch)
        {
            if (finished == false)
                spriteBatch.Draw(spriteStrip, destinationRect, sourceRect, Color.White);
        }
    }
}
