﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TankCity
{
    class Enemy : MovingObject
    {
        //Texture
        public List<Texture2D> Textures;

        //Health of Enemy
        public int Health;
        public bool HealthRemoved;

        //Reload delay
        public int Reload = 50;

        //Enemy Type
        public int EnemyType;

        //Randomiser
        Random r;

        public void Initialize(Vector2 position, Vector2 velocity, int speed, int direction, List<Texture2D> textures, int width, int height, int health, int type)
        {
            Position = position;
            PrevPosition = position;
            Velocity = velocity;
            Speed = speed;
            Direction = direction;
            Textures = textures;
            Width = width;
            Height = height;
            Health = health;
            HealthRemoved = false;
            EnemyType = type;
            ObjectType = 2;
            r = new Random();
        }
        public void ChangeDirection()
        {
            Direction = r.Next(1, 5);
            if (Direction == 1)
                Velocity = new Vector2(0, -Speed);
            else if (Direction == 2)
                Velocity = new Vector2(Speed, 0);
            else if (Direction == 3)
                Velocity = new Vector2(0, Speed);
            else
                Velocity = new Vector2(-Speed, 0);
        }
    }
}
