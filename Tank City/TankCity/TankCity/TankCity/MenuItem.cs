﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TankCity
{
    class MenuItem
    {
        //Position
        public Vector2 Position;

        //Dimensions
        public int Width;
        public int Height;

        //State
        public int State; //0 = not selected, 1 = selected

        //Instruction
        public string Instruction;

        //Textures
        public Texture2D TextureNonActive;
        public Texture2D TextureActive;

        //constructor
        public MenuItem(Vector2 position, int width, int height, string instruction, Texture2D texture1, Texture2D texture2)
        {
            Position = position;
            Width = width;
            Height = height;
            Instruction = instruction;
            State = 0;
            TextureNonActive = texture1;
            TextureActive = texture2;
        }

    }
}
