﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TankCity
{
    class MovingObject
    {

        //Position vector
        public Vector2 Position;
        public Vector2 PrevPosition;

        //Dimensions
        public int Width;
        public int Height;

        //Velocity vector
        public Vector2 Velocity;

        //Direction
        public int Direction;

        //Speed
        public int Speed;

        //Object type
        public int ObjectType;

        //returning a rectangle for collision detection
        public Rectangle GetRectangle()
        {
            Rectangle rect = new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
            return rect;
        }
    }
}
