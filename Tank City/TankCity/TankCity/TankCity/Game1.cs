/* PTRCON001
 * Connor Patrick
 * Tank City clone
 * 1 March 2013 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace TankCity
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Textures
        Texture2D groundTexture;
        Texture2D wallTexture;
        Texture2D waterTexture;
        Texture2D treeTexture;
        Texture2D stoneTexture;
        Texture2D explosionTexture;
        List<Texture2D> enemy1Textures;
        List<Texture2D> enemy2Textures;
        List<Texture2D> enemy3Textures;
        Texture2D baseTexture;
        Texture2D boomTexture;
        List<Texture2D> bulletTextures;
        SpriteFont SpriteFont1;
        SpriteFont EndGameFont;

        //Menu textures and buttons
        Texture2D backgroundTexture;
        Texture2D start1Texture;
        Texture2D start2Texture;
        Texture2D exit1Texture;
        Texture2D exit2Texture;
        Texture2D end1Texture;
        Texture2D end2Texture;
        Texture2D resume1Texture;
        Texture2D resume2Texture;
        MenuItem start;
        MenuItem exit;
        MenuItem resume;
        MenuItem end;
        List<MenuItem> buttons1;
        List<MenuItem> buttons2;
        int currentButton;
        int buttonDelay;

        //Sounds
        SoundEffect bulletFire;
        SoundEffect explosionSound;
        SoundEffect baseBoom;
        Song gameplayMusic;
        Song menuMusic;

        //Variable for drawing the background
        Rectangle groundSrcRect;
        int tilesAcross;
        int tilesDown;

        //Player information
        Player player1;
        Player player2;
        char player1Command;
        char player2Command;

        //Map array
        int[,] map;
        List<Rectangle> stone;
        List<Rectangle> water;
        List<Rectangle> walls;
        List<Rectangle> trees;
        Rectangle baseRect;

        //Collision grid
        List<MovingObject>[] grid;

        //List of projectiles
        List<Projectile> projectiles;

        //List of explosions
        List<Explosion> explosions;

        //List of enemies
        List<Enemy> enemies;
        int timer;
        int wave;
        int spawned;

        //Random number generator
        Random random;

        //Game state
        int state; //0 = Main menu, 1 = playing, 2 = paused, 3 = game over

        //Buttons

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 900;
            graphics.PreferredBackBufferHeight = 512;
            graphics.IsFullScreen = false;
            Content.RootDirectory = "Content";
        }

        //Initializes the game
        protected override void Initialize()
        {
            //setting information required for drawing the background
            groundSrcRect = new Rectangle(0, 0, 80, 80);
            tilesAcross = 512 / groundSrcRect.Width + 1;
            tilesDown = 512 / groundSrcRect.Height + 1;

            //variables used for enemies
            timer = 0;
            wave = 50;
            spawned = 0;

            //The collision grid
            grid = new List<MovingObject>[16];

            //Initializing the players
            player1 = new Player();
            player2 = new Player();

            //Initializing projectile list and the enemies list
            projectiles = new List<Projectile>();
            enemies = new List<Enemy>();

            //An array dipicting the map
            map = new int[32, 32]
            {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 1, 1, 0, 0, 4, 4, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 1, 1, 0, 0, 4, 4, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 1, 1, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 1, 1, 2, 2, 0, 0, 1, 1},
                {1, 1, 3, 3, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 3, 3, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 2, 2, 3, 3, 2, 2, 1, 1, 1, 1},
                {1, 1, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 2, 2, 3, 3, 2, 2, 1, 1, 1, 1},
                {1, 1, 0, 0, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0, 1, 1, 4, 4, 3, 3, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0, 1, 1, 4, 4, 3, 3, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 4, 4, 4, 4, 3, 3, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 4, 4, 4, 4, 3, 3, 2, 2, 0, 0, 1, 1},
                {1, 1, 1, 1, 2, 2, 0, 0, 1, 1, 3, 3, 2, 2, 0, 0, 0, 0, 2, 2, 4, 4, 4, 4, 3, 3, 2, 2, 0, 0, 1, 1},
                {1, 1, 1, 1, 2, 2, 0, 0, 1, 1, 3, 3, 2, 2, 0, 0, 0, 0, 2, 2, 4, 4, 4, 4, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 2, 2, 1, 1, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 1, 1, 2, 2, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 2, 2, 5, 6, 6, 6, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 2, 2, 6, 6, 6, 6, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 6, 6, 6, 6, 2, 2, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 6, 6, 6, 6, 2, 2, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
            };
            //Creating lists of each object, containing rectangles that define their position
            stone = new List<Rectangle>();
            water = new List<Rectangle>();
            walls = new List<Rectangle>();
            trees = new List<Rectangle>();

            //Lists for enemy textures
            enemy1Textures = new List<Texture2D>();
            enemy2Textures = new List<Texture2D>();
            enemy3Textures = new List<Texture2D>();

            //Initializing the random number generator
            random = new Random();

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] == 1)
                        stone.Add(new Rectangle(j * 16 + 194, i * 16, 16, 16));
                    else if (map[i, j] == 3)
                        trees.Add(new Rectangle(j * 16 + 194, i * 16, 16, 16));
                    else if (map[i, j] == 4)
                        water.Add(new Rectangle(j * 16 + 194, i * 16, 16, 16));
                    else if (map[i, j] == 5)
                        baseRect = new Rectangle(j * 16 + 194, i * 16, 64, 64);
                }
            }

            explosions = new List<Explosion>();

            //Update game state
            state = 0;
            currentButton = 0;
            buttonDelay = 10;

            base.Initialize();
        }

        //Loads all content required by game
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Loading the textures
            groundTexture = Content.Load<Texture2D>("Textures/Ground");
            wallTexture = Content.Load<Texture2D>("Textures/wall");
            stoneTexture = Content.Load<Texture2D>("Textures/stone");
            treeTexture = Content.Load<Texture2D>("Textures/tree");
            waterTexture = Content.Load<Texture2D>("Textures/water");
            explosionTexture = Content.Load<Texture2D>("Textures/explosion");
            bulletTextures = new List<Texture2D>();
            for (int i = 1; i < 5; i++)
            {
                player1.textures.Add(Content.Load<Texture2D>("Textures/Tank1/Tank" + i));
                player2.textures.Add(Content.Load<Texture2D>("Textures/Tank2/Tank2" + i));
                bulletTextures.Add(Content.Load<Texture2D>("Textures/Bullets/Bullet" + i));
                enemy1Textures.Add(Content.Load<Texture2D>("Textures/Enemies/Enemy1/Enemy1" + i));
                enemy2Textures.Add(Content.Load<Texture2D>("Textures/Enemies/Enemy2/Enemy2" + i));
                enemy3Textures.Add(Content.Load<Texture2D>("Textures/Enemies/Enemy3/Enemy3" + i));
            }
            baseTexture = Content.Load<Texture2D>("Textures/base");
            boomTexture = Content.Load<Texture2D>("Textures/Boom");
            SpriteFont1 = Content.Load<SpriteFont>("SpriteFont1");
            EndGameFont = Content.Load<SpriteFont>("EndGameFont");
            backgroundTexture = Content.Load<Texture2D>("Textures/Background");
            start1Texture = Content.Load<Texture2D>("Textures/Buttons/Start1");
            start2Texture = Content.Load<Texture2D>("Textures/Buttons/Start2");
            exit1Texture = Content.Load<Texture2D>("Textures/Buttons/Exit1");
            exit2Texture = Content.Load<Texture2D>("Textures/Buttons/Exit2");
            resume1Texture = Content.Load<Texture2D>("Textures/Buttons/Resume1");
            resume2Texture = Content.Load<Texture2D>("Textures/Buttons/Resume2");
            end1Texture = Content.Load<Texture2D>("Textures/Buttons/End1");
            end2Texture = Content.Load<Texture2D>("Textures/Buttons/End2");

            //Initializing buttons
            start = new MenuItem(new Vector2(375, 100), 150, 45, "start", start1Texture, start2Texture);
            exit = new MenuItem(new Vector2(375, 155), 150, 45, "exit", exit1Texture, exit2Texture);
            resume = new MenuItem(new Vector2(375, 100), 150, 45, "resume", resume1Texture, resume2Texture);
            end = new MenuItem(new Vector2(375, 155), 150, 45, "exit", end1Texture, end2Texture);
            buttons1 = new List<MenuItem>();
            buttons1.Add(start);
            buttons1.Add(exit);
            buttons2 = new List<MenuItem>();
            buttons2.Add(resume);
            buttons2.Add(end);

            //loading sounds
            gameplayMusic = Content.Load<Song>("Sounds/gameMusic");
            menuMusic = Content.Load<Song>("Sounds/menuMusic");
            baseBoom = Content.Load<SoundEffect>("Sounds/baseBoom");
            explosionSound = Content.Load<SoundEffect>("Sounds/explosion");
            bulletFire = Content.Load<SoundEffect>("Sounds/tankFire");
            PlayMusic(menuMusic);

            //Initializing player positions now that sprite size is known
            player1.Initialize(new Vector2(226, GraphicsDevice.Viewport.Bounds.Height - 64), 32, 32);
            player2.Initialize(new Vector2(642, GraphicsDevice.Viewport.Bounds.Height - 64), 32, 32);
        }

        //Unloads content
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        //Updates the game (game logic)
        protected override void Update(GameTime gameTime)
        {
            //updating the button delay
            buttonDelay--;

            //Handling the updates based on state of the game
            if (state == 0)
            {
                //handling input in main menu
                buttons1[currentButton].State = 1;
                if (buttonDelay <= 0)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.Up) || GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0)
                    {
                        if (currentButton > 0)
                        {
                            buttons1[currentButton].State = 0;
                            currentButton--;
                        }
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Down) || GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < 0)
                    {
                        if (currentButton < (buttons1.Count - 1))
                        {
                            buttons1[currentButton].State = 0;
                            currentButton++;
                        }
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Enter) || GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
                    {
                        if (buttons1[currentButton].Instruction == "exit")
                            this.Exit();
                        else if (buttons1[currentButton].Instruction == "start")
                        {
                            state = 1;
                            PlayMusic(gameplayMusic);
                            currentButton = 0;
                        }
                    }
                    buttonDelay = 10;
                }
            }

            else if (state == 1)
            {
                //pause buttons in-game
                if (buttonDelay <= 0)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.Escape) || GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed || GamePad.GetState(PlayerIndex.Two).Buttons.Start == ButtonState.Pressed)
                        state = 2;
                    buttonDelay = 10;
                }

                //checking if wave is finished
                if (spawned == wave && enemies.Count == 0)
                {
                    state = 3;
                    MediaPlayer.Stop();
                }

                //Checking if players are out of lives
                if (player1.Lives <= 0 || player2.Lives <= 0)
                {
                    state = 3;
                    MediaPlayer.Stop();
                }

                //Updating explosions
                for (int exp = 0; exp < explosions.Count; exp++)
                {
                    explosions[exp].Update(gameTime);
                    //Removing dead ones
                    if (explosions[exp].finished == true)
                        explosions.RemoveAt(exp);
                }

                //Updating the map to account for missing walls
                walls.Clear();
                for (int i = 0; i < map.GetLength(0); i++)
                {
                    for (int j = 0; j < map.GetLength(1); j++)
                    {
                        if (map[i, j] == 2)
                            walls.Add(new Rectangle(j * 16 + 194, i * 16, 16, 16));
                    }
                }
                //Update enemies
                UpdateEnemies();

                //Updating the players
                UpdatePlayers();

                //Checking collisions
                CheckCollision();
            }

            else if (state == 2)
            {
                //checking delay to make sure a single keypress doesn't count as many
                //handling input in the in-game menu
                if (buttonDelay <= 0)
                {
                    buttons2[currentButton].State = 1;
                    if (Keyboard.GetState().IsKeyDown(Keys.Escape) || GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed || GamePad.GetState(PlayerIndex.Two).Buttons.Start == ButtonState.Pressed)
                    {
                        state = 1;
                        currentButton = 0;
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.Up) || GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0)
                    {
                        if (currentButton > 0)
                        {
                            buttons2[currentButton].State = 0;
                            currentButton--;
                        }
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.Down) || GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < 0)
                    {
                        if (currentButton < (buttons2.Count - 1))
                        {
                            buttons2[currentButton].State = 0;
                            currentButton++;
                        }
                    }
                    else if (Keyboard.GetState().IsKeyDown(Keys.Enter) || GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
                    {
                        if (buttons2[currentButton].Instruction == "exit")
                        {
                            Initialize();
                        }
                        else if (buttons2[currentButton].Instruction == "resume")
                            state = 1;
                        currentButton = 0;
                    }
                    buttonDelay = 10;
                }
            }
            else
            {
                //The end game screen. Can only go back to the main menu
                if (buttonDelay <= 0)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.Escape) || GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
                        Initialize();
                }
            }

            base.Update(gameTime);
        }

        //Plays the game music
        private void PlayMusic(Song song)
        {
            MediaPlayer.Stop();
            try
            {
                MediaPlayer.Play(song);
                MediaPlayer.IsRepeating = true;
            }
            catch{}
        }

        //Updates enemies
        private void UpdateEnemies()
        {
            //Updating their previous position variables
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].PrevPosition = enemies[i].Position;
            }

            //Spawning random enemies randomly at three seperate places
            timer--;
            if (spawned < wave)
            {
                if (enemies.Count < 10)
                {
                    if (timer <= 0)
                    {
                        int enemyType = random.Next(1, 6);
                        int enemySpawn = random.Next(1, 4);
                        Enemy enemy = new Enemy();
                        if (enemySpawn == 1)
                        {
                            //check if spawning is possible first
                            bool noSpawn = false;
                            //making sure no other enemies are in the way
                            if (grid[0] != null)
                            {
                                for (int i = 0; i < grid[0].Count; i++)
                                {
                                    if ((grid[0][i].Position.X >= 32 + 194 && grid[0][i].Position.X <= 64 + 194) || (grid[0][i].Position.X + grid[0][i].Width >= 32 + 194 && grid[0][i].Position.X + grid[0][i].Width <= 64 + 194))
                                    {
                                        if ((grid[0][i].Position.Y >= 32 && grid[0][i].Position.Y <= 64) || (grid[0][i].Position.Y + grid[0][i].Height >= 32 && grid[0][i].Position.Y + grid[0][i].Height <= 64))
                                        {
                                            noSpawn = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            //making sure the players are not in the way
                            if ((player1.Position.X >= 32 + 194 && player1.Position.X < 64 + 194) || (player1.Position.X + player1.Width >= 32 + 194 && player1.Position.X + player1.Width < 64 + 194))
                            {
                                if ((player1.Position.Y >= 32 && player1.Position.Y < 64) || (player1.Position.Y + player1.Height >= 32 && player1.Position.Y + player1.Height < 64))
                                    noSpawn = true;
                            }
                            if ((player2.Position.X >= 32 + 194 && player2.Position.X < 64 + 194) || (player2.Position.X + player2.Width >= 32 + 194 && player2.Position.X + player2.Width < 64 + 194))
                            {
                                if ((player2.Position.Y >= 32 && player2.Position.Y < 64) || (player2.Position.Y + player2.Height >= 32 && player2.Position.Y + player2.Height < 64))
                                    noSpawn = true;
                            }
                            //spawining the enemy if it is safe to do so
                            if (noSpawn == false)
                            {
                                if (enemyType == 1 || enemyType == 2 || enemyType == 3)
                                    enemy.Initialize(new Vector2(32 + 194, 32), new Vector2(0, 1), 1, 3, enemy1Textures, enemy1Textures[0].Bounds.Width, enemy1Textures[0].Bounds.Height, 1, 1);
                                else if (enemyType == 4)
                                    enemy.Initialize(new Vector2(32 + 194, 32), new Vector2(0, 2), 2, 3, enemy2Textures, enemy2Textures[0].Bounds.Width, enemy2Textures[0].Bounds.Height, 1, 2);
                                else
                                    enemy.Initialize(new Vector2(32 + 194, 32), new Vector2(0, 1), 1, 3, enemy3Textures, enemy3Textures[0].Bounds.Width, enemy3Textures[0].Bounds.Height, 3, 3);
                                enemies.Add(enemy);
                                spawned++;
                                timer = 250;
                            }
                        }
                        else if (enemySpawn == 2)
                        {
                            //check if spawning is possible first
                            bool noSpawn = false;
                            //making sure the players are not in the way
                            if (grid[1] != null)
                            {
                                for (int i = 0; i < grid[1].Count; i++)
                                {
                                    if ((grid[1][i].Position.X >= 224 + 194 && grid[1][i].Position.X <= 256 + 194) || (grid[1][i].Position.X + grid[1][i].Width >= 224 + 194 && grid[1][i].Position.X + grid[1][i].Width <= 256 + 194))
                                    {
                                        if ((grid[1][i].Position.Y >= 32 && grid[1][i].Position.Y <= 64) || (grid[1][i].Position.Y + grid[1][i].Height >= 32 && grid[1][i].Position.Y + grid[1][i].Height <= 64))
                                        {
                                            noSpawn = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            //making sure the players are not in the way
                            if ((player1.Position.X >= 224 + 194 && player1.Position.X < 256 + 194) || (player1.Position.X + player1.Width >= 224 + 194 && player1.Position.X + player1.Width < 256 + 194))
                            {
                                if ((player1.Position.Y >= 32 && player1.Position.Y < 64) || (player1.Position.Y + player1.Height >= 32 && player1.Position.Y + player1.Height < 64))
                                    noSpawn = true;
                            }
                            if ((player2.Position.X >= 224 + 194 && player2.Position.X < 256 + 194) || (player2.Position.X + player2.Width >= 224 + 194 && player2.Position.X + player2.Width < 256 + 194))
                            {
                                if ((player2.Position.Y >= 32 && player2.Position.Y < 64) || (player2.Position.Y + player2.Height >= 32 && player2.Position.Y + player2.Height < 64))
                                    noSpawn = true;
                            }
                            //spawining the enemy if it is safe to do so
                            if (noSpawn == false)
                            {
                                if (enemyType == 1 || enemyType == 2 || enemyType == 3)
                                    enemy.Initialize(new Vector2(224 + 194, 32), new Vector2(0, 1), 1, 3, enemy1Textures, enemy1Textures[0].Bounds.Width, enemy1Textures[0].Bounds.Height, 1, 1);
                                else if (enemyType == 4)
                                    enemy.Initialize(new Vector2(224 + 194, 32), new Vector2(0, 2), 2, 3, enemy2Textures, enemy2Textures[0].Bounds.Width, enemy2Textures[0].Bounds.Height, 1, 2);
                                else
                                    enemy.Initialize(new Vector2(224 + 194, 32), new Vector2(0, 1), 1, 3, enemy3Textures, enemy3Textures[0].Bounds.Width, enemy3Textures[0].Bounds.Height, 3, 3);
                                enemies.Add(enemy);
                                spawned++;
                                timer = 250;
                            }
                        }
                        else
                        {
                            //check if spawning is possible first
                            bool noSpawn = false;
                            //making sure the players are not in the way
                            if (grid[3] != null)
                            {
                                for (int i = 0; i < grid[3].Count; i++)
                                {
                                    if ((grid[3][i].Position.X >= 448 + 194 && grid[3][i].Position.X <= 480 + 194) || (grid[3][i].Position.X + grid[3][i].Width >= 448 + 194 && grid[3][i].Position.X + grid[3][i].Width <= 480 + 194))
                                    {
                                        if ((grid[3][i].Position.Y >= 32 && grid[3][i].Position.Y <= 64) || (grid[3][i].Position.Y + grid[3][i].Height >= 32 && grid[3][i].Position.Y + grid[3][i].Height <= 64))
                                        {
                                            noSpawn = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            //making sure the players are not in the way
                            if ((player1.Position.X >= 448 + 194 && player1.Position.X < 480 + 194) || (player1.Position.X + player1.Width >= 448 + 194 && player1.Position.X + player1.Width < 480 + 194))
                            {
                                if ((player1.Position.Y >= 32 && player1.Position.Y < 64) || (player1.Position.Y + player1.Height >= 32 && player1.Position.Y + player1.Height < 64))
                                    noSpawn = true;
                            }
                            if ((player2.Position.X >= 448 + 194 && player2.Position.X < 480 + 194) || (player2.Position.X + player2.Width >= 448 + 194 && player2.Position.X + player2.Width < 480 + 194))
                            {
                                if ((player2.Position.Y >= 32 && player2.Position.Y < 64) || (player2.Position.Y + player2.Height >= 32 && player2.Position.Y + player2.Height < 64))
                                    noSpawn = true;
                            }
                            //spawining the enemy if it is safe to do so
                            if (noSpawn == false)
                            {
                                if (enemyType == 1 || enemyType == 2 || enemyType == 3)
                                    enemy.Initialize(new Vector2(448 + 194, 32), new Vector2(0, 1), 1, 3, enemy1Textures, enemy1Textures[0].Bounds.Width, enemy1Textures[0].Bounds.Height, 1, 1);
                                else if (enemyType == 4)
                                    enemy.Initialize(new Vector2(448 + 194, 32), new Vector2(0, 2), 2, 3, enemy2Textures, enemy2Textures[0].Bounds.Width, enemy2Textures[0].Bounds.Height, 1, 2);
                                else
                                    enemy.Initialize(new Vector2(448 + 194, 32), new Vector2(0, 1), 1, 3, enemy3Textures, enemy3Textures[0].Bounds.Width, enemy3Textures[0].Bounds.Height, 3, 3);
                                enemies.Add(enemy);
                                spawned++;
                                timer = 250;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < enemies.Count; i++)
            {
                //making enemies fire 
                enemies[i].Reload = enemies[i].Reload - 1;
                if (enemies[i].Reload <= 0)
                {
                    Projectile bullet = new Projectile();
                    bullet.Speed = 3;
                    if (enemies[i].Direction == 1)
                        bullet.Initialize(new Vector2(enemies[i].Position.X + (enemies[i].Width / 2) - 4, enemies[i].Position.Y + (enemies[i].Height / 2) - 7), 8, 14, bulletTextures[0], new Vector2(0, -bullet.Speed), 3);
                    else if (enemies[i].Direction == 2)
                        bullet.Initialize(new Vector2(enemies[i].Position.X + (enemies[i].Width / 2) - 7, enemies[i].Position.Y + (enemies[i].Height / 2) - 4), 14, 8, bulletTextures[1], new Vector2(bullet.Speed, 0), 3);
                    else if (enemies[i].Direction == 3)
                        bullet.Initialize(new Vector2(enemies[i].Position.X + (enemies[i].Width / 2) - 4, enemies[i].Position.Y + (enemies[i].Height / 2) - 7), 8, 14, bulletTextures[2], new Vector2(0, bullet.Speed), 3);
                    else
                        bullet.Initialize(new Vector2(enemies[i].Position.X + (enemies[i].Width / 2) - 7, enemies[i].Position.Y + (enemies[i].Height / 2) - 4), 14, 8, bulletTextures[3], new Vector2(-bullet.Speed, 0), 3);
                    projectiles.Add(bullet);
                    bulletFire.Play();
                    enemies[i].Reload = 80;
                }
                //Removing dead enemies
                if (enemies[i].Health <= 0)
                {
                    Explosion explosion = new Explosion();
                    explosion.Initialize(explosionTexture, enemies[i].Position, 134, 134, 12, 45);
                    explosions.Add(explosion);
                    explosionSound.Play();
                    enemies.RemoveAt(i);
                }
            }

            //Moving them
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].Position += enemies[i].Velocity;
            }
        }

        //Updates players
        private void UpdatePlayers()
        {
            //Keeping track of players' previous positions
            player1.PrevPosition = player1.Position;
            player2.PrevPosition = player2.Position;

            //Updating the reload timers
            player1.Reload--;
            player2.Reload--;

            //variables used to limit input to only one thing (dpad, keyboard, joystick)
            bool p1Input = false;
            bool p2Input = false;

            //Left joystick controls
            float X1 = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X * player1.Speed;
            float Y1 = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y * player1.Speed;
            float X2 = GamePad.GetState(PlayerIndex.Two).ThumbSticks.Left.X * player2.Speed;
            float Y2 = GamePad.GetState(PlayerIndex.Two).ThumbSticks.Left.Y * player2.Speed;
            //setting direction of player 1 based on joystick
            if (Math.Abs(X1) > Math.Abs(Y1))
            {
                if (X1 > 0)
                    player1Command = 'R';
                else if (X1 < 0)
                    player1Command = 'L';
            }
            else
            {
                if (Y1 > 0)
                    player1Command = 'U';
                else if (Y1 < 0)
                    player1Command = 'D';
            }
            //setting direction of player 2 based on joystick
            if (Math.Abs(X2) > Math.Abs(Y2))
            {
                if (X2 > 0)
                    player2Command = 'R';
                else if (X2 < 0)
                    player2Command = 'L';
            }
            else
            {
                if (Y2 > 0)
                    player2Command = 'U';
                else if (Y2 < 0)
                    player2Command = 'D';
            }

            //checking if there was joystick input
            if (X1 != 0 || Y1 != 0)
                p1Input = true;
            if (X2 != 0 || Y2 != 0)
                p2Input = true;
            
            //Keyboard and DPad controls for movement
            if (p1Input == false) //just checking whether input is still required first
            {
                if (Keyboard.GetState().IsKeyDown(Keys.A) || GamePad.GetState(PlayerIndex.One).DPad.Left == ButtonState.Pressed)
                        player1Command = 'L';
                if (Keyboard.GetState().IsKeyDown(Keys.D) || GamePad.GetState(PlayerIndex.One).DPad.Right == ButtonState.Pressed)
                        player1Command = 'R';
                if (Keyboard.GetState().IsKeyDown(Keys.W) || GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed)
                        player1Command = 'U';
                if (Keyboard.GetState().IsKeyDown(Keys.S) || GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed)
                        player1Command = 'D';
            }
            if (p2Input == false) //just checking whether input is still required first
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Left) || GamePad.GetState(PlayerIndex.Two).DPad.Left == ButtonState.Pressed)
                        player2Command = 'L';
                if (Keyboard.GetState().IsKeyDown(Keys.Right) || GamePad.GetState(PlayerIndex.Two).DPad.Right == ButtonState.Pressed)
                        player2Command = 'R';
                if (Keyboard.GetState().IsKeyDown(Keys.Up) || GamePad.GetState(PlayerIndex.Two).DPad.Up == ButtonState.Pressed)
                        player2Command = 'U';
                if (Keyboard.GetState().IsKeyDown(Keys.Down) || GamePad.GetState(PlayerIndex.Two).DPad.Down == ButtonState.Pressed)
                        player2Command = 'D';
            }

            //firing of bullets
            if (Keyboard.GetState().IsKeyDown(Keys.Space) || GamePad.GetState(PlayerIndex.One).IsButtonDown(Buttons.A))
            {
                if (player1.Reload <= 0) //checking cooldown time
                {
                    Projectile bullet = new Projectile();
                    bullet.Speed = 3;
                    if (player1.Direction == 1)
                        bullet.Initialize(new Vector2(player1.Position.X + (player1.Width / 2) - 4, player1.Position.Y + (player1.Height / 2) - 7), 8, 14, bulletTextures[0], new Vector2(0, -bullet.Speed), 1);
                    else if (player1.Direction == 2)
                        bullet.Initialize(new Vector2(player1.Position.X + (player1.Width / 2) - 7, player1.Position.Y + (player1.Height / 2) - 4), 14, 8, bulletTextures[1], new Vector2(bullet.Speed, 0), 1);
                    else if (player1.Direction == 3)
                        bullet.Initialize(new Vector2(player1.Position.X + (player1.Width / 2) - 4, player1.Position.Y + (player1.Height / 2) - 7), 8, 14, bulletTextures[2], new Vector2(0, bullet.Speed), 1);
                    else
                        bullet.Initialize(new Vector2(player1.Position.X + (player1.Width / 2) - 7, player1.Position.Y + (player1.Height / 2) - 4), 14, 8, bulletTextures[3], new Vector2(-bullet.Speed, 0), 1);
                    projectiles.Add(bullet);
                    bulletFire.Play();
                    player1.Reload = 40;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter) || GamePad.GetState(PlayerIndex.Two).IsButtonDown(Buttons.A))
            {
                if (player2.Reload <= 0) //checking cooldown time
                {
                    Projectile bullet2 = new Projectile();
                    bullet2.Speed = 3;
                    if (player2.Direction == 1)
                        bullet2.Initialize(new Vector2(player2.Position.X + (player2.Width / 2) - 4, player2.Position.Y + (player2.Height / 2) - 7), 8, 14, bulletTextures[0], new Vector2(0, -bullet2.Speed), 2);
                    else if (player2.Direction == 2)
                        bullet2.Initialize(new Vector2(player2.Position.X + (player2.Width / 2) - 7, player2.Position.Y + (player2.Height / 2) - 4), 14, 8, bulletTextures[1], new Vector2(bullet2.Speed, 0), 2);
                    else if (player2.Direction == 3)
                        bullet2.Initialize(new Vector2(player2.Position.X + (player2.Width / 2) - 4, player2.Position.Y + (player2.Height / 2) - 7), 8, 14, bulletTextures[2], new Vector2(0, bullet2.Speed), 2);
                    else
                        bullet2.Initialize(new Vector2(player2.Position.X + (player2.Width / 2) - 7, player2.Position.Y + (player2.Height / 2) - 4), 14, 8, bulletTextures[3], new Vector2(-bullet2.Speed, 0), 2);
                    projectiles.Add(bullet2);
                    bulletFire.Play();
                    player2.Reload = 40;
                }
            }

            //Setting velocity based on button commands
            if (player1Command != ' ')
            {
                if (((player1.Position.X + player1.Width/2) - 194) % 16 < 1)
                {
                    if ((player1.Position.Y + player1.Height/2) % 16 < 1)
                    {
                        switch (player1Command)
                        {
                            case 'L':
                                player1.Velocity = new Vector2(-player1.Speed, 0);
                                player1.Direction = 4;
                                player1Command = ' ';
                                break;
                            case 'R':
                                player1.Velocity = new Vector2(player1.Speed, 0);
                                player1.Direction = 2;
                                player1Command = ' ';
                                break;
                            case 'D':
                                player1.Velocity = new Vector2(0, player1.Speed);
                                player1.Direction = 3;
                                player1Command = ' ';
                                break;
                            case 'U':
                                player1.Velocity = new Vector2(0, -player1.Speed);
                                player1.Direction = 1;
                                player1Command = ' ';
                                break;
                        }
                    }
                }
            }
            //moving player 1 and allowing for the pause motion button to work
            if (!Keyboard.GetState().IsKeyDown(Keys.LeftControl) && GamePad.GetState(PlayerIndex.One).Triggers.Left == 0)
            {
                player1.Position += player1.Velocity;
            }
            if (player2Command != ' ')
            {
                if (((player2.Position.X + player2.Width / 2) - 194)% 16 < 1)
                {
                    if ((player2.Position.Y + player2.Height / 2) % 16 < 1)
                    {
                        switch (player2Command)
                        {
                            case 'L':
                                player2.Velocity = new Vector2(-player2.Speed, 0);
                                player2.Direction = 4;
                                player2Command = ' ';
                                break;
                            case 'R':
                                player2.Velocity = new Vector2(player2.Speed, 0);
                                player2.Direction = 2;
                                player2Command = ' ';
                                break;
                            case 'D':
                                player2.Velocity = new Vector2(0, player2.Speed);
                                player2.Direction = 3;
                                player2Command = ' ';
                                break;
                            case 'U':
                                player2.Velocity = new Vector2(0, -player2.Speed);
                                player2.Direction = 1;
                                player2Command = ' ';
                                break;
                        }
                    }
                }
            }
            //moving player 2 and allowing for motion to be paused
            if (!Keyboard.GetState().IsKeyDown(Keys.RightControl) && GamePad.GetState(PlayerIndex.Two).Triggers.Left == 0)
            {
                player2.Position += player2.Velocity;
            }
            //moving the new projectiles
            for (int i = 0; i < projectiles.Count; i++)
            {
                projectiles[i].Position += projectiles[i].Velocity;
            }
        }

        //Handles collision detection
        private void CheckCollision()
        {
            //Collisions against environment
            //get map blocks that player is in and then just check against what is already there
            int p1X1 = (int)(player1.Position.X - 194) / 16;
            int p1X2 = (int)(player1.Position.X + player1.Width - 195) / 16;
            int p1Y1 = (int)player1.Position.Y / 16;
            int p1Y2 = (int)(player1.Position.Y + player1.Height - 1) / 16;
            if ((map[p1Y1, p1X1] != 0 && map[p1Y1, p1X1] != 3) || (map[p1Y1, p1X2] != 0 && map[p1Y1, p1X2] != 3) || (map[p1Y2, p1X1] != 0 && map[p1Y2, p1X1] != 3) || (map[p1Y2, p1X2] != 0 && map[p1Y2, p1X2] != 3))
                player1.Position = player1.PrevPosition;
            //player2
            int p2X1 = (int)(player2.Position.X - 194) / 16;
            int p2X2 = (int)(player2.Position.X + player2.Width - 195) / 16;
            int p2Y1 = (int)player2.Position.Y / 16;
            int p2Y2 = (int)(player2.Position.Y + player2.Height - 1) / 16;
            if ((map[p2Y1, p2X1] != 0 && map[p2Y1, p2X1] != 3) || (map[p2Y1, p2X2] != 0 && map[p2Y1, p2X2] != 3) || (map[p2Y2, p2X1] != 0 && map[p2Y2, p2X1] != 3) || (map[p2Y2, p2X2] != 0 && map[p2Y2, p2X2] != 3))
                player2.Position = player2.PrevPosition;

            //Enemies against environment
            for (int i = 0; i < enemies.Count; i++)
            {
                int X1 = (int)(enemies[i].Position.X - 194) / 16;
                int X2 = (int)(enemies[i].Position.X + enemies[i].Width - 195) / 16;
                int Y1 = (int)enemies[i].Position.Y / 16;
                int Y2 = (int)(enemies[i].Position.Y + enemies[i].Height - 1) / 16;
                if ((map[Y1, X1] != 0 && map[Y1, X1] != 3) || (map[Y1, X2] != 0 && map[Y1, X2] != 3) || (map[Y2, X1] != 0 && map[Y2, X1] != 3) || (map[Y2, X2] != 0 && map[Y2, X2] != 3))
                {
                    enemies[i].Position = enemies[i].PrevPosition;
                    enemies[i].ChangeDirection();
                }
            }

            //Bullets against environment
            for (int i = 0; i < projectiles.Count; i++)
            {
                int x1 = (int)(projectiles[i].Position.X - 194) / 16;
                int x2 = (int)(projectiles[i].Position.X + projectiles[i].Width - 195) / 16;
                int y1 = (int)projectiles[i].Position.Y / 16;
                int y2 = (int)(projectiles[i].Position.Y + projectiles[i].Height - 1) / 16;
                bool hit = false;
                if (map[y1, x2] == 2)
                {
                    map[y1, x2] = 0;
                    hit = true;
                }
                if (map[y2, x2] == 2)
                {
                    map[y2, x2] = 0;
                    hit = true;
                }
                if (map[y1, x1] == 2)
                {
                    map[y1, x1] = 0;
                    hit = true;
                }
                if (map[y2, x1] == 2)
                {
                    map[y2, x1] = 0;
                    hit = true;
                }
                if (map[y1, x1] == 1 || map[y2, x1] == 1 || map[y1, x2] == 1 || map[y2, x2] == 1)
                    hit = true;
                if (map[y1, x1] == 5 || map[y1, x1] == 6 || map[y2, x1] == 5 || map[y2, x1] == 6 || map[y1, x2] == 5 || map[y1, x2] == 5 || map[y2, x2] == 6 || map[y2, x2] == 6)
                {
                    hit = true;
                    if (projectiles[i].ProjectileType == 3)
                    {
                        baseBoom.Play();
                        state = 3;
                    }
                }

                if (hit == true)
                    projectiles.RemoveAt(i);
            }
    
            //check all moving objects against each other
            //reseting the lists in the grid array
            for (int i = 0; i < grid.Length; i++)
                grid[i] = new List<MovingObject>();
            
            //adding objects to grid
            //enemies
            for (int i = 0; i < enemies.Count; i++)
            {
                int X1 = (int)(enemies[i].Position.X - 194)/ 128;
                int X2 = ((int)enemies[i].Position.X + enemies[i].Width - 194) / 128;
                int Y1 = (int)enemies[i].Position.Y / 128;
                int Y2 = ((int)enemies[i].Position.Y + enemies[i].Height) / 128;
                grid[X1 + (Y1 * 4)].Add(enemies[i]);
                if (X2 != X1)
                    grid[X2 + (Y1 * 4)].Add(enemies[i]);
                if (Y2 != Y1)
                    grid[X1 + (Y2 * 4)].Add(enemies[i]);
                if (Y2 != Y1 && X2 != X1)
                    grid[X2 + (Y2 * 4)].Add(enemies[i]);
            }
            //players
            p1X1 = p1X1 * 16 / 128;
            p1X2 = p1X2 * 16 / 128;
            p1Y1 = p1Y1 * 16 / 128;
            p1Y2 = p1Y2 * 16 / 128;
            grid[p1X1 + (p1Y1 * 4)].Add(player1);
            if (p1X2 != p1X1)
                grid[p1X2 + (p1Y1 * 4)].Add(player1);
            if (p1Y2 != p1Y1)
                grid[p1X1 + (p1Y2 * 4)].Add(player1);
            if (p1Y2 != p1Y1 && p1X2 != p1X1)
                grid[p1X2 + (p1Y2 * 4)].Add(player1);
            p2X1 = p2X1 * 16 / 128;
            p2X2 = p2X2 * 16 / 128;
            p2Y1 = p2Y1 * 16 / 128;
            p2Y2 = p2Y2 * 16 / 128;
            grid[p2X1 + (p2Y1 * 4)].Add(player2);
            if (p2X2 != p2X1)
                grid[p2X2 + (p2Y1 * 4)].Add(player2);
            if (p2Y2 != p2Y1)
                grid[p2X1 + (p2Y2 * 4)].Add(player2);
            if (p2Y2 != p2Y1 && p2X2 != p2X1)
                grid[p2X2 + (p2Y2 * 4)].Add(player2);
            //projectiles
            for (int i = 0; i < projectiles.Count; i++)
            {
                int X1 = (int)(projectiles[i].Position.X - 194)/ 128;
                int X2 = ((int)projectiles[i].Position.X + projectiles[i].Width - 194) / 128;
                int Y1 = (int)projectiles[i].Position.Y / 128;
                int Y2 = ((int)projectiles[i].Position.Y + projectiles[i].Height) / 128;
                grid[X1 + (Y1 * 4)].Add(projectiles[i]);
                if (X2 != X1)
                    grid[X2 + (Y1 * 4)].Add(projectiles[i]);
                if (Y2 != Y1)
                    grid[X1 + (Y2 * 4)].Add(projectiles[i]);
                if (Y2 != Y1 && X2 != X1)
                    grid[X2 + (Y2 * 4)].Add(projectiles[i]);
            }

            //colliding objects in grid
            for (int i = 0; i < grid.Length; i++)
            {
                if (grid[i].Count > 1)
                {
                    for (int j = 0; j < grid[i].Count; j++)
                    {
                        for (int k = j + 1; k < grid[i].Count; k++)
                        {
                            if (grid[i][j].GetRectangle().Intersects(grid[i][k].GetRectangle()))
                            {
                                //projectile vs projectile collision
                                if (grid[i][j].GetType() == typeof(Projectile) && grid[i][k].GetType() == typeof(Projectile))
                                {
                                    Projectile p1 = grid[i][j] as Projectile;
                                    Projectile p2 = grid[i][k] as Projectile;
                                    p1.delete = true;
                                    p2.delete = true;
                                }
                                //enemy vs player collision
                                else if ((grid[i][j].GetType() == typeof(Enemy) && grid[i][k].GetType() == typeof(Player)) || (grid[i][j].GetType() == typeof(Player) && grid[i][k].GetType() == typeof(Enemy)))
                                {
                                    grid[i][j].Position = grid[i][j].PrevPosition;
                                    grid[i][j].Velocity = -grid[i][j].Velocity;
                                    grid[i][k].Position = grid[i][k].PrevPosition;
                                    grid[i][k].Velocity = -grid[i][k].Velocity;
                                }
                                //enemy vs enemy collision
                                else if (grid[i][j].GetType() == typeof(Enemy) && grid[i][k].GetType() == typeof(Enemy))
                                {
                                    Enemy e1 = grid[i][j] as Enemy;
                                    Enemy e2 = grid[i][k] as Enemy;
                                    e1.Position = e1.PrevPosition;
                                    e2.Position = e2.PrevPosition;
                                    e1.Velocity = -e1.Velocity;
                                    e2.Velocity = -e2.Velocity;
                                    e1.Direction += 2;
                                    e2.Direction += 2;
                                    if (e1.Direction == 5)
                                        e1.Direction = 1;
                                    else if (e1.Direction == 6)
                                        e1.Direction = 2;
                                    if (e2.Direction == 5)
                                        e2.Direction = 1;
                                    else if (e2.Direction == 6)
                                        e2.Direction = 2;
                                }
                                //projectile vs enemy collision
                                else if (grid[i][j].GetType() == typeof(Projectile) && grid[i][k].GetType() == typeof(Enemy))
                                {
                                    Projectile p = grid[i][j] as Projectile;
                                    if (p.ProjectileType != 3)
                                    {
                                        p.delete = true;
                                        Enemy e = grid[i][k] as Enemy;
                                        e.Health--;
                                        if (e.Health <= 0)
                                        {
                                            if (p.ProjectileType == 1)
                                                player1.Score += e.EnemyType * 10;
                                            else
                                                player2.Score += e.EnemyType * 10;
                                        }
                                    }
                                }
                                //projectile vs enemy collision part 2 (different order in containers)
                                else if (grid[i][j].GetType() == typeof(Enemy) && grid[i][k].GetType() == typeof(Projectile))
                                {
                                    Projectile p = grid[i][k] as Projectile;
                                    if (p.ProjectileType != 3)
                                    {
                                        p.delete = true;
                                        Enemy e = grid[i][j] as Enemy;
                                        e.Health--;
                                        if (e.Health <= 0)
                                        {
                                            if (p.ProjectileType == 1)
                                                player1.Score += e.EnemyType * 10;
                                            else
                                                player2.Score += e.EnemyType * 10;
                                        }
                                    }
                                }
                                //projectile vs player collision
                                else if (grid[i][j].GetType() == typeof(Projectile) && grid[i][k].GetType() == typeof(Player))
                                {
                                    Projectile p = grid[i][j] as Projectile;
                                    if (p.ProjectileType == 3)
                                    {
                                        p.delete = true;
                                        Player pl = grid[i][k] as Player;
                                        pl.Lives--;
                                        Explosion explosion = new Explosion();
                                        explosion.Initialize(explosionTexture, pl.Position, 134, 134, 12, 45);
                                        explosions.Add(explosion);
                                        explosionSound.Play();
                                        if (pl.InitPosition.X < 250)
                                        {
                                            for (int obj = 0; obj < grid[12].Count; obj++)
                                            {
                                                if ((grid[12][obj].Position.X >= pl.InitPosition.X && grid[12][obj].Position.X < pl.InitPosition.X + pl.Width) && (grid[12][obj].Position.Y >= pl.Position.Y))
                                                    grid[12].RemoveAt(obj);
                                            }
                                        }
                                        else
                                        {
                                            for (int obj = 0; obj < grid[15].Count; obj++)
                                            {
                                                if ((grid[15][obj].Position.X + grid[15][obj].Width >= pl.InitPosition.X && grid[15][obj].Position.X + grid[15][obj].Width < pl.InitPosition.X + pl.Width) && (grid[15][obj].Position.Y >= pl.Position.Y))
                                                    grid[15].RemoveAt(obj);
                                            }
                                        }
                                        //respawing player
                                        pl.Position = pl.InitPosition;
                                    }
                                }
                                //player vs projectile collision
                                else if (grid[i][j].GetType() == typeof(Player) && grid[i][k].GetType() == typeof(Projectile))
                                {
                                    Projectile p = grid[i][k] as Projectile;
                                    if (p.ProjectileType == 3)
                                    {
                                        p.delete = true;
                                        Player pl = grid[i][j] as Player;
                                        pl.Lives--;
                                        Explosion explosion = new Explosion();
                                        explosion.Initialize(explosionTexture, pl.Position, 134, 134, 12, 45);
                                        explosions.Add(explosion);
                                        explosionSound.Play();
                                        if (pl.InitPosition.X < 250)
                                        {
                                            for (int obj = 0; obj < grid[12].Count; obj++)
                                            {
                                                if ((grid[12][obj].Position.X >= pl.InitPosition.X && grid[12][obj].Position.X < pl.InitPosition.X + pl.Width) && (grid[12][obj].Position.Y >= pl.Position.Y))
                                                    grid[12].RemoveAt(obj);
                                            }
                                        }
                                        else
                                        {
                                            for (int obj = 0; obj < grid[15].Count; obj++)
                                            {
                                                if ((grid[15][obj].Position.X + grid[15][obj].Width >= pl.InitPosition.X && grid[15][obj].Position.X + grid[15][obj].Width < pl.InitPosition.X + pl.Width) && (grid[15][obj].Position.Y >= pl.Position.Y))
                                                    grid[15].RemoveAt(obj);
                                            }
                                        }
                                        //respawning player
                                        pl.Position = pl.InitPosition;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //removing dead bullets
            for (int i = 0; i < projectiles.Count; i++)
            {
                if (projectiles[i].delete == true)
                    projectiles.RemoveAt(i);
            }
        }

        //Drawing game elements
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            //drawing the main menu
            if (state == 0)
            {
                spriteBatch.Draw(backgroundTexture, new Rectangle(194,0,512,512),Color.White);
                for (int i = 0; i < buttons1.Count; i++)
                {
                    if (buttons1[i].State == 0)
                        spriteBatch.Draw(buttons1[i].TextureNonActive, new Rectangle((int)buttons1[i].Position.X, (int)buttons1[i].Position.Y, buttons1[i].Width, buttons1[i].Height), Color.White);
                    else
                        spriteBatch.Draw(buttons1[i].TextureActive, new Rectangle((int)buttons1[i].Position.X, (int)buttons1[i].Position.Y, buttons1[i].Width, buttons1[i].Height), Color.White);
                }
            }

            //mid-game menu
            else if (state == 2)
            {
                spriteBatch.Draw(backgroundTexture, new Rectangle(194, 0, 512, 512), Color.White);
                for (int i = 0; i < buttons2.Count; i++)
                {
                    if (buttons2[i].State == 0)
                        spriteBatch.Draw(buttons2[i].TextureNonActive, new Rectangle((int)buttons2[i].Position.X, (int)buttons2[i].Position.Y, buttons2[i].Width, buttons2[i].Height), Color.White);
                    else
                        spriteBatch.Draw(buttons2[i].TextureActive, new Rectangle((int)buttons2[i].Position.X, (int)buttons2[i].Position.Y, buttons2[i].Width, buttons2[i].Height), Color.White);
                }
            }

            //drawing the game
            else
            {
                //Drawing the background
                for (int i = 0; i < tilesAcross - 1; i++)
                {
                    for (int j = 0; j < tilesDown; j++)
                    {
                        spriteBatch.Draw(groundTexture, new Rectangle(i * groundSrcRect.Width + 194, j * groundSrcRect.Height, groundSrcRect.Width, groundSrcRect.Height), Color.White);
                    }
                }
                //Drawing the headquarters/base
                spriteBatch.Draw(baseTexture, baseRect, Color.White);
  
                //Drawing the stone
                for (int iS = 0; iS < stone.Count; iS++)
                    spriteBatch.Draw(stoneTexture, stone[iS], Color.White);
                //Drawing the walls
                for (int iW = 0; iW < walls.Count; iW++)
                    spriteBatch.Draw(wallTexture, walls[iW], Color.White);
                //Drawing the water
                for (int iWa = 0; iWa < water.Count; iWa++)
                    spriteBatch.Draw(waterTexture, water[iWa], Color.White);

                //Drawing the players
                spriteBatch.Draw(player2.textures[player2.Direction - 1], new Rectangle((int)player2.Position.X, (int)player2.Position.Y, player2.Width, player2.Height), Color.White);
                spriteBatch.Draw(player1.textures[player1.Direction - 1], new Rectangle((int)player1.Position.X, (int)player1.Position.Y, player1.Width, player1.Height), Color.White);

                //Drawing the enemies
                for (int i = 0; i < enemies.Count; i++)
                {
                    spriteBatch.Draw(enemies[i].Textures[enemies[i].Direction - 1], new Rectangle((int)enemies[i].Position.X, (int)enemies[i].Position.Y, enemies[i].Width, enemies[i].Height), Color.White);
                }

                //Drawing the bullets
                for (int i = 0; i < projectiles.Count; i++)
                {
                    spriteBatch.Draw(projectiles[i].Texture, new Rectangle((int)projectiles[i].Position.X, (int)projectiles[i].Position.Y, projectiles[i].Width, projectiles[i].Height), Color.White);
                }

                //Drawing the explosions
                for (int i = 0; i < explosions.Count; i++)
                    explosions[i].Draw(spriteBatch);

                //Drawing the trees
                for (int iT = 0; iT < trees.Count; iT++)
                    spriteBatch.Draw(treeTexture, trees[iT], Color.White);

                //Drawing the scores
                string p1Score = "" + player1.Score;
                string p2Score = "" + player2.Score;
                while (p2Score.Length < 4)
                {
                    p2Score = " " + p2Score;
                }
                spriteBatch.DrawString(SpriteFont1, p1Score, new Vector2(225, 483), Color.LimeGreen);
                spriteBatch.DrawString(SpriteFont1, p2Score, new Vector2(631, 483), Color.MediumAquamarine);

                //Drawing the players' lives
                for (int i = 0; i < player1.Lives; i++)
                    spriteBatch.Draw(player1.textures[0], new Rectangle(296 + (i * 30), 483, 25, 25), Color.White);
                for (int i = 0; i < player2.Lives; i++)
                    spriteBatch.Draw(player2.textures[0], new Rectangle(576 - (i * 30), 483, 25, 25), Color.White);

                //Drawing the number of enemies remaining
                for (int i = 0; i < (wave - spawned); i++)
                    spriteBatch.Draw(enemy3Textures[0], new Rectangle(200 + i * 10, 10, 10, 10), Color.White);
           
                //end-game results
                if (state == 3)
                {

                    //Drawing explosion
                    if (player1.Lives > 0 && player2.Lives > 0 && enemies.Count != 0)
                        spriteBatch.Draw(boomTexture, new Rectangle(350, 280, 200, 200), Color.White);
                    spriteBatch.DrawString(EndGameFont, "Game Over", new Vector2(15, 30), Color.Red);
                    spriteBatch.DrawString(EndGameFont, "Game Over", new Vector2(720, 30), Color.Red);
                    if ((player1.Lives > 0 && player2.Lives <= 0) || (player2.Lives > 0 && player1.Lives <= 0))
                    {
                        if (player1.Lives <= 0)
                        {
                            spriteBatch.DrawString(EndGameFont, "Player 1", new Vector2(15, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, "Failed!!", new Vector2(15, 295), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, "Player 2", new Vector2(720, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, " Wins!! ", new Vector2(720, 295), Color.Orange);
                        }
                        else
                        {
                            spriteBatch.DrawString(EndGameFont, "Player 1", new Vector2(15, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, " Wins!! ", new Vector2(15, 295), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, "Player 2", new Vector2(720, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, "Failed!!", new Vector2(720, 295), Color.Orange);
                        }
                    }
                    else
                    {
                        if (player1.Score > player2.Score)
                        {
                            spriteBatch.DrawString(EndGameFont, "Player 1", new Vector2(15, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, " Wins!! ", new Vector2(15, 295), Color.Orange);
                        }
                        else if (player1.Score < player2.Score)
                        {
                            spriteBatch.DrawString(EndGameFont, "Player 2", new Vector2(720, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, " Wins!! ", new Vector2(720, 295), Color.Orange);
                        }
                        else
                        {
                            spriteBatch.DrawString(EndGameFont, " Tie!! ", new Vector2(15, 255), Color.Orange);
                            spriteBatch.DrawString(EndGameFont, " Tie!! ", new Vector2(720, 255), Color.Orange);
                        }
                    }
                }
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
