﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TankCity
{
    class Projectile : MovingObject
    {
        //Projectile damage
        public int Damage;

        //Projectile Type 
        public int ProjectileType; //1 = player1, 2 = player2, 3 = enemy

        //Texture
        public Texture2D Texture;

        //mark for deletion
        public bool delete = false;

        public void Initialize(Vector2 position, int width, int height, Texture2D texture, Vector2 velocity, int type)
        {
            Position = position;
            Width = width;
            Height = height;
            Velocity = velocity;
            Texture = texture;
            Speed = 3;
            Damage = 1;
            ProjectileType = type;
            ObjectType = 3;
        }
    }
}
